#!/bin/bash
ip token
echo " "
echo "Ingresa alguna de las interfaces de arriba:"; read interfaz # Pide y lee la interfaz de Internet.
echo "¿Qué tipo de seguridad tiene tu red? (wpa/wep):"; read tipo # Pide y lee el tipo de seguridad de la red Wi-Fi.
echo "Ingresa el nombre de la red:"; read ssid # Pide y lee la SSID de la red WPA.
echo "Ingresa la clave:"; read key # Pide y lee la clave de la red.

sudo chmod a+w /etc/network/interfaces # Abre los permisos de escritura de /etc/network/interfaces para editarlo.

if [ "$tipo" = "wpa" ] || [ "$tipo" = "WPA" ] || [ "$tipo" = "Wpa" ]; then
if [ "$ssid" = "" ] && [ "$key" = "" ] ; then # Si $ssid y $key están vacías, no se imprimirá en /etc/network/interfaces. Esto es más que nada para las redes Ethernet
  cat << EOF > /etc/network/interfaces # Reescribe el archivo con los nuevos datos.
# Este archivo está autogenerado con un ejecutable que yo hize.
# https://gitlab.com/bruwalfas/wifire/

source /etc/network/interfaces.d/*

auto $interfaz
iface $interfaz inet dhcp
EOF

else # Si $ssid y $key no están vacías, las pondrán en el archivo

cat << EOF > /etc/network/interfaces # Reescribe el archivo con los nuevos datos.
# Este archivo está autogenerado con un ejecutable que yo hize.
# https://gitlab.com/bruwalfas/wifire/

source /etc/network/interfaces.d/*

auto $interfaz
iface $interfaz inet dhcp
wpa-ssid "$ssid"
wpa-psk "$key"
EOF

fi
elif [ "$tipo" = "wep" ] || [ "$tipo" = "WEP" ] || [ "$tipo" = "Wep" ]; then
  if [ "$ssid" = "" ] && [ "$key" = "" ] ; then # Si $ssid y $key están vacías, no se imprimirá en /etc/network/interfaces. Esto es más que nada para las redes Ethernet
    cat << EOF > /etc/network/interfaces # Reescribe el archivo con los nuevos datos.
# Este archivo está autogenerado con un ejecutable que yo hize.
# https://gitlab.com/bruwalfas/wifire/

source /etc/network/interfaces.d/*

auto $interfaz
iface $interfaz inet dhcp
EOF

else # Si $ssid y $key no están vacías, las pondrán en el archivo

cat << EOF > /etc/network/interfaces # Reescribe el archivo con los nuevos datos.
# Este archivo está autogenerado con un ejecutable que yo hize.
# https://gitlab.com/bruwalfas/wifire/

source /etc/network/interfaces.d/*

auto $interfaz
iface $interfaz inet dhcp
wireless-essid "$ssid"
wireless-key "$key"
EOF

  fi
elif [ "$tipo" = "" ]; then
  cat << EOF > /etc/network/interfaces # Reescribe el archivo con los nuevos datos.
# Este archivo está autogenerado con un ejecutable que yo hize.
# https://gitlab.com/bruwalfas/wifire/

source /etc/network/interfaces.d/*

auto $interfaz
iface $interfaz inet dhcp
EOF
else
  echo "Debes ingresar 'wep' o 'wpa'."
  echo " "
  wifire
fi
sudo chmod a-w /etc/network/interfaces; sudo chmod u+w /etc/network/interfaces # Cierra los permisos de escritura de /etc/network/interfaces

sudo ifdown $interfaz && sudo ifup $interfaz #Reinicia la interfaz.
# Gracias por usar mi ejecutable. No dudes en dar una sugerencia/arreglo al código, está bajo la GPLv3 :P
